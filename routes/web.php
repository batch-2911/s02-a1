<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'featuredPosts']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::get('/posts/create', [PostController::class, 'createPost'])->name('posts-create');

Route::post('/post', [PostController::class, 'storePost'])->name('posts-store');

// define a route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'showPost'])->name('post.index');

Route::delete('/posts/{postid}', [PostController::class, 'deletePost'])->name('posts-delete');

Route::get('/myPost', [PostController::class, 'myPosts'])->name('myposts');

Route::get('/post/{postid}', [PostController::class,'showPostById'])->name('post.show');

Route::get('/post/{postid}/edit', [PostController::class, 'editPost'])->name('post.edit');

Route::put('/post/{postid}', [PostController::class, 'updatePost'])->name('posts.update');
