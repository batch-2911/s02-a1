@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('posts-store'); }}">
        @csrf;
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" id="content" class="form-control" id="content" name="content" rows="3"></textarea>
        </div>

        <div>
            <button type="submit" class="btn btn-primary">Create Post</button>
        </div>


    </form>
@endsection